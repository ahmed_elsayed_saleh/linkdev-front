import AppCache from "../../data/cache/AppCache";
import {Route, Redirect} from 'react-router-dom';

export default function MustBeLoginAuth(props) {
    return <Route render={(p) => {
        const isUserLogin = AppCache.getInstance().hasUser();
        if (isUserLogin) {
            return <props.component {...p}/>
        }
        return <Redirect to={"/auth/login"}/>
    }}/>
}