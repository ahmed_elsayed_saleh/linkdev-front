import AppCache from "../../data/cache/AppCache";
import {Redirect, Route} from 'react-router-dom';

export default function MustBeGuestGuard(props) {
    return <Route render={(p) => {
        const isUserLogin = AppCache.getInstance().hasUser();
        if (isUserLogin) {
            return <Redirect to={"/"}/>
        }
        return <props.component {...p}/>
    }}/>
}