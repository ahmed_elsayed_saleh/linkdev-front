import {Badge, Button, Card, Col, Container, Form, ListGroup, ListGroupItem, Row} from "react-bootstrap";
import {useEffect, useMemo, useRef, useState} from "react";
import socketIOClient from "socket.io-client";
import AppCache from "../../../data/cache/AppCache";
import {ForLoop, IfCondition} from "react-ifloop";
import moment from "moment";
import {useHistory} from "react-router-dom/cjs/react-router-dom";
import Config from "../../../config/Config";

export default function ChatComponent() {
    // Ui Data.
    const [messages, setMessages] = useState([]);
    const [liveUsers, setLiveUsers] = useState([]);
    const user = useMemo(() => {
        return AppCache.getInstance().getUser();
    }, []);

    let socketRef = useRef();
    const inputRef = useRef();
    const routerHistory = useHistory();

    useEffect(() => {
        socketRef.current = socketIOClient(Config.WEBSOCKET_URL, {
            transportOptions: {
                polling: {
                    extraHeaders: {
                        'authorization': user.token
                    }
                }
            }
        });

        // listen to messages from server.
        socketRef.current?.on("new_message", newMessage => {
            setMessages((oldMessages) => {
                return [...oldMessages, newMessage];
            });
        });
        socketRef.current?.on("messages", newMessages => {
            setMessages(newMessages);
        });
        socketRef.current?.on("live_users", (users) => {
            setLiveUsers(users);
        });

        socketRef.current?.on("connect_error", (error) => {
            if (error.data.type === "authentication_error") {
                AppCache.getInstance().clearCache();
                routerHistory.replace("/auth/login");
            }
        });

        return () => socketRef.current?.disconnect();
        // eslint-disable-next-line
    }, []);

    const sendMessage = (message) => {
        socketRef.current.emit("new_message", message);
    }

    return <Container className={"mt-4"}>
        <Card>
            <Card.Header>
                Chat Room
            </Card.Header>
            <Card.Body>
                <Row>
                    <Col md={9}>
                        <Row className={"flex-column"}>
                            <Col>
                                <div style={{maxHeight: "600px", overflowY: "scroll"}}>
                                    <IfCondition condition={messages.length > 0}>
                                        <ListGroup variant={"flush"}>
                                            <ForLoop items={messages} forEachItem={(item, index) => {
                                                return <ListGroupItem key={item._id}>
                                                    <div
                                                        className={item.fromUserId === user._id ? "text-right" : "text-left"}>
                                                        <h6 className={"text-danger mb-0 font-weight-bold"}>{item.fromUserName}</h6>
                                                        <p className={"mb-0 mt-0"}>{item.message}</p>
                                                        <Badge variant={"success"} pill>{
                                                            moment(item.createdAt).fromNow()}</Badge>
                                                    </div>
                                                </ListGroupItem>
                                            }}/>
                                        </ListGroup>
                                    </IfCondition>
                                </div>
                            </Col>
                            <Col>
                                <hr/>
                                <Row>
                                    <Col md={10}>
                                        <Form.Control placeholder={"message"} ref={inputRef}/>
                                    </Col>
                                    <Col>
                                        <Button onClick={() => {
                                            const value = inputRef.current?.value;
                                            if (value && value.trim() !== "") {
                                                sendMessage(value.trim());
                                                inputRef.current.value = "";
                                            }
                                        }
                                        } variant={"success"}>SEND</Button>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                    <Col style={{"borderLeft": "1px solid #ccc"}}>
                        <ListGroup>
                            <ForLoop items={liveUsers} forEachItem={(liveUser) => {
                                return <ListGroup.Item key={liveUser._id}
                                                       variant={(liveUser._id === user._id) ? "success" : "info"}>{liveUser.name}</ListGroup.Item>
                            }}/>
                        </ListGroup>
                    </Col>
                </Row>
            </Card.Body>
        </Card>
    </Container>
}