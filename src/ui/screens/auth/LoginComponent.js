import {Button, Card, Container, Form, Spinner} from "react-bootstrap";
import {Link, useHistory} from "react-router-dom";
import {useFormik} from "formik";
import * as Yup from "yup";
import {IfCondition} from "react-ifloop";
import HttpRequest from "../../../data/network/HttpRequest";
import InfoDialogComponent from "../../components/InfoDialogComponent";
import {useState} from "react";
import HttpService from "../../../data/network/HttpService";
import User from "../../../data/model/User";
import AppCache from "../../../data/cache/AppCache";

export default function LoginComponent() {
    const router = useHistory();
    const formik = useFormik({
        validateOnChange: false,
        validateOnBlur: false,
        initialValues: {
            email: "",
            password: ""
        },
        validationSchema: Yup.object().shape({
            email: Yup.string().email().required(),
            password: Yup.string()
                .min(6, "Min length 6 characters")
                .required("Please enter password")
        }),
        async onSubmit(values) {
            // call web service.
            const request = new HttpRequest();
            request.url = "api/v1/auth/login";
            request.body = values;
            request.method = 'POST';
            setLoading(true);
            HttpService.send(request).then((response) => {
                const user = new User(response.data.user);
                AppCache.getInstance().saveUser(user);
                router.replace("/");
            }).catch((e) => {
                setLoading(false);
                setInfo(Object.assign({}, info, {message: e.message, show: true}));
            });
        },
    });

    const onConfirmDialogClick = () => {
        setInfo(Object.assign({}, info, {show: false}));
    }

    const [isLoading, setLoading] = useState(false);
    const [info, setInfo] = useState({show: false, onConfirm: onConfirmDialogClick, message: ""});

    return <Container>
        <Form className={"mt-4"} onSubmit={formik.handleSubmit}>
            <Card>
                <Card.Header>
                    Welcome to App [Login]
                </Card.Header>
                <Card.Body>
                    <Form.Group>
                        <Form.Label htmlFor={"email"}>Email Address</Form.Label>
                        <Form.Control placeholder={"Email Address"} id={"email"} type={"email"} name={"email"}
                                      {...formik.getFieldProps("email")}/>
                        <IfCondition condition={formik.errors.email != null}>
                            <Form.Text className={"text-danger"}>{formik.errors.email}</Form.Text>
                        </IfCondition>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor={"password"}>Password</Form.Label>
                        <Form.Control placeholder={"Password"} id={"password"} type={"password"} name={"password"}
                                      {...formik.getFieldProps("password")}/>
                        <IfCondition condition={formik.errors.password != null}>
                            <Form.Text className={"text-danger"}>{formik.errors.password}</Form.Text>
                        </IfCondition>
                    </Form.Group>
                    <h6>Don't have an account? <Link to={'/auth/register'} className={"text-primary"}>Register
                        One</Link></h6>
                    <div className={"text-center"}>
                        <Button onClick={formik.submitForm} variant={"primary"} disabled={isLoading}>LOGIN</Button>
                        <IfCondition condition={isLoading}>
                            <div className={'mt-2'}>
                                <Spinner animation="border" variant="success"/>
                            </div>
                        </IfCondition>
                    </div>
                </Card.Body>
            </Card>
        </Form>
        <InfoDialogComponent message={info.message} onConfirm={info.onConfirm} show={info.show}/>
    </Container>
}