import {Link, useHistory} from "react-router-dom";
import {useFormik} from "formik";
import * as Yup from "yup";
import HttpRequest from "../../../data/network/HttpRequest";
import HttpService from "../../../data/network/HttpService";
import {useState} from "react";
import {Button, Card, Container, Form, Spinner} from "react-bootstrap";
import {IfCondition} from "react-ifloop";
import InfoDialogComponent from "../../components/InfoDialogComponent";

export default function RegisterComponent() {

    const router = useHistory();
    const formik = useFormik({
        validateOnChange: false,
        validateOnBlur: false,
        initialValues: {
            name: "",
            email: ""
        },
        validationSchema: Yup.object().shape({
            email: Yup.string().email().required(),
            name: Yup.string()
                .min(4, "Min length 4 characters")
                .required("Please enter name")
        }),
        async onSubmit(values) {
            // call web service.
            const request = new HttpRequest();
            request.url = "api/v1/auth/register";
            request.body = values;
            request.method = 'POST';
            setLoading(true);
            HttpService.send(request).then((response) => {
                router.replace("/welcome");
            }).catch((e) => {
                setLoading(false);
                setInfo(Object.assign({}, info, {message: e.message, show: true}));
            });
        },
    });

    const onConfirmDialogClick = () => {
        setInfo(Object.assign({}, info, {show: false}));
    }

    const [isLoading, setLoading] = useState(false);
    const [info, setInfo] = useState({show: false, onConfirm: onConfirmDialogClick, message: ""});

    return <Container>
        <Form className={"mt-4"} onSubmit={formik.handleSubmit}>
            <Card>
                <Card.Header>
                    Welcome to App [Register]
                </Card.Header>
                <Card.Body>
                    <Form.Group>
                        <Form.Label htmlFor={"name"}>Name</Form.Label>
                        <Form.Control placeholder={"Name"} id={"name"} type={"text"} name={"name"}
                                      {...formik.getFieldProps("name")}/>
                        <IfCondition condition={formik.errors.name != null}>
                            <Form.Text className={"text-danger"}>{formik.errors.name}</Form.Text>
                        </IfCondition>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label htmlFor={"email"}>Email Address</Form.Label>
                        <Form.Control placeholder={"Email Address"} id={"email"} type={"email"} name={"email"}
                                      {...formik.getFieldProps("email")}/>
                        <IfCondition condition={formik.errors.email != null}>
                            <Form.Text className={"text-danger"}>{formik.errors.email}</Form.Text>
                        </IfCondition>
                    </Form.Group>
                    <h6>Already have an account? <Link to={'/auth/login'} className={"text-primary"}>Login</Link></h6>
                    <div className={"text-center"}>
                        <Button onClick={formik.submitForm} variant={"primary"} disabled={isLoading}>Register</Button>
                        <IfCondition condition={isLoading}>
                            <div className={'mt-2'}>
                                <Spinner animation="border" variant="success"/>
                            </div>
                        </IfCondition>
                    </div>
                </Card.Body>
            </Card>
        </Form>
        <InfoDialogComponent message={info.message} onConfirm={info.onConfirm} show={info.show}/>
    </Container>
}