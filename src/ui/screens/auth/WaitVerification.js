import {Alert, Container} from "react-bootstrap";

export default function WaitVerification() {

    return <Container className={"mt-4"}>
        <Alert variant={"success"}>
            <Alert.Heading>
                Thanks for registration
            </Alert.Heading>
            <hr/>
            <p>We send email verification to your email address , check your email inbox</p>
        </Alert>
    </Container>
}