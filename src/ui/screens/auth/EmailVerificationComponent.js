import {Link, useHistory} from "react-router-dom";
import {useFormik} from "formik";
import * as Yup from "yup";
import HttpRequest from "../../../data/network/HttpRequest";
import HttpService from "../../../data/network/HttpService";
import {useState} from "react";
import {Button, Card, Container, Form, Spinner} from "react-bootstrap";
import {IfCondition} from "react-ifloop";
import InfoDialogComponent from "../../components/InfoDialogComponent";
import QueryParamsUtil from "../../../util/QueryParamsUtil";

export default function EmailVerificationComponent(props) {
    const urlQuery = QueryParamsUtil.getParams(props.location.search);
    const router = useHistory();
    const formik = useFormik({
        validateOnChange: false,
        validateOnBlur: false,
        initialValues: {
            password: "",
            retypedPassword: ""
        },
        validationSchema: Yup.object().shape({
            password: Yup.string()
                .min(6, "Min length 6 characters")
                .required("Please enter password"),
            retypedPassword: Yup.string()
                .oneOf([Yup.ref('password')], 'Passwords must match')
                .min(6, "Min length 6 characters")
                .required("Please enter password")
        }),
        async onSubmit(values) {
            // call web service.
            const request = new HttpRequest();
            request.url = "api/v1/auth/emailVerification";
            request.body = {verificationCode: urlQuery.token, newPassword: values.password};
            request.method = 'PUT';
            setLoading(true);
            HttpService.send(request).then((response) => {
                router.replace("/");
            }).catch((e) => {
                setLoading(false);
                setInfo(Object.assign({}, info, {message: e.message, show: true}));
            });
        },
    });

    const onConfirmDialogClick = () => {
        setInfo(Object.assign({}, info, {show: false}));
    }

    const [isLoading, setLoading] = useState(false);
    const [info, setInfo] = useState({show: false, onConfirm: onConfirmDialogClick, message: ""});

    return <Container>
        <Form className={"mt-4"} onSubmit={formik.handleSubmit}>
            <Card>
                <Card.Header>
                    Welcome to App [Email Verification]
                </Card.Header>
                <Card.Body>
                    <Form.Group>
                        <Form.Label htmlFor={"password"}>Password</Form.Label>
                        <Form.Control placeholder={"Password"} id={"password"} type={"password"} name={"password"}
                                      {...formik.getFieldProps("password")}/>
                        <IfCondition condition={formik.errors.password != null}>
                            <Form.Text className={"text-danger"}>{formik.errors.password}</Form.Text>
                        </IfCondition>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor={"retypedPassword"}>Retyped Password</Form.Label>
                        <Form.Control placeholder={"Retyped Password"} id={"retypedPassword"} type={"password"}
                                      name={"retypedPassword"}
                                      {...formik.getFieldProps("retypedPassword")}/>
                        <IfCondition condition={formik.errors.retypedPassword != null}>
                            <Form.Text className={"text-danger"}>{formik.errors.retypedPassword}</Form.Text>
                        </IfCondition>
                    </Form.Group>
                    <h6>Don't have an account? <Link to={'/auth/register'} className={"text-primary"}>Register
                        One</Link></h6>
                    <div className={"text-center"}>
                        <Button onClick={formik.submitForm} variant={"primary"} disabled={isLoading}>Verify</Button>
                        <IfCondition condition={isLoading}>
                            <div className={'mt-2'}>
                                <Spinner animation="border" variant="success"/>
                            </div>
                        </IfCondition>
                    </div>
                </Card.Body>
            </Card>
        </Form>
        <InfoDialogComponent message={info.message} onConfirm={info.onConfirm} show={info.show}/>
    </Container>
}