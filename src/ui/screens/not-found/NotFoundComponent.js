import "./not-found.css";

export default function NotFoundComponent() {
    return (<div id="main">
        <div className="fof">
            <h1>Error 404</h1>
        </div>
    </div>)
}