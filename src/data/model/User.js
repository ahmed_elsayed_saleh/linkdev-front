export default class User {
    constructor(data) {
        this._id = data['_id'];
        this.name = data['name'];
        this.email = data['email'];
        this.isVerified = data['isVerified'] || false;
        this.createdAt = data['createdAt'];
        this.token = data['token'];
    }

    toJson = () => ({
        _id: this._id,
        name: this.name,
        email: this.email,
        isVerified: this.isVerified,
        createdAt: this.createdAt,
        token: this.token
    });
}