import axios from "axios";
import AppCache from "../cache/AppCache";
import Config from "../../config/Config";

// Add a request interceptor
axios.interceptors.request.use(
    function (config) {
        const loginUser = AppCache.getInstance().getUser();
        if (loginUser && loginUser.token) {
            config.headers.authorization = `Bearer ${loginUser.token}`;
        }
        return config;
    },
    function (error) {
        // Do something with request error
        return Promise.reject(error);
    }
);

// Add a response interceptor
axios.interceptors.response.use(
    function (response) {
        // Do something with response data
        return response;
    },
    function (error) {
        // Do something with response error
        const exceptionToThrow = {};
        if (error.response) {
            // error has response.
            exceptionToThrow.message = error.response.data.message;
            exceptionToThrow.statusCode = error.response.data.statusCode;
            exceptionToThrow.serverCode = error.response.status;
        } else {
            // error not has response , error in server or timeout.
            exceptionToThrow.message = "Error in server , try again later";
            exceptionToThrow.statusCode = "";
            exceptionToThrow.serverCode = 500;
        }

        return Promise.reject(exceptionToThrow);
    }
);

export default class HttpService {
    /**
     * @param {HttpRequest} request
     */
    static send(request) {
        const url = HttpService.getConnectionUrl(request);
        switch (request.method) {
            case "POST" :
                return axios.post(url, request.body, {
                    headers: request.headers,
                    params: request.queryParams,
                });
            case "GET" :
                return axios.get(url, {
                    headers: request.headers,
                    params: request.queryParams,
                });
            case "PUT" :
                return axios.put(url, request.body, {
                    headers: request.headers,
                    params: request.queryParams,
                });
            case "DELETE":
                return axios.delete(url, {
                    headers: request.headers,
                    params: request.queryParams,
                });
            default:
                throw new Error("request method must be added before send request.");
        }
    }

    /**
     * @param {HttpRequest} request
     */
    static getConnectionUrl(request) {
        const baseUrl = (request.baseUrl != null && request.baseUrl !== "") ? request.baseUrl : Config.HTTP_URL;
        let apiUrl = request.url;
        if (!apiUrl.startsWith("/")) {
            apiUrl = "/" + apiUrl;
        }
        return baseUrl + apiUrl;
    }
}