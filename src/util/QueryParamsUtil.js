import queryString from 'query-string';


export default class QueryParamsUtil {

    static getParams(query) {
        return queryString.parse(query);
    }
}