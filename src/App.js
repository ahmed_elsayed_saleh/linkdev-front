import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import MustBeGuestGuard from "./ui/components/MustBeGuestGuard";
import LoginComponent from "./ui/screens/auth/LoginComponent";
import MustBeLoginAuth from "./ui/components/MustBeLoginAuth";
import ChatComponent from "./ui/screens/chat/ChatComponent";
import NotFoundComponent from "./ui/screens/not-found/NotFoundComponent";
import WaitVerification from "./ui/screens/auth/WaitVerification";
import RegisterComponent from "./ui/screens/auth/RegisterComponent";
import EmailVerificationComponent from "./ui/screens/auth/EmailVerificationComponent";

function App() {
    return (
        <div>
            <BrowserRouter>
                <Switch>
                    <MustBeGuestGuard path={"/auth/login"} component={LoginComponent} exact={true}/>
                    <MustBeGuestGuard path={"/auth/register"} component={RegisterComponent} exact={true}/>
                    <MustBeGuestGuard path={'/auth/emailVerification'} component={EmailVerificationComponent}/>
                    <MustBeLoginAuth path={"/"} component={ChatComponent} exact={true}/>
                    <Route component={WaitVerification} path={"/welcome"}/>
                    <Route component={NotFoundComponent}/>
                </Switch>
            </BrowserRouter>
        </div>
    );
}

export default App;
