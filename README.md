# Task Documentation

Project is organized with packaging by feature and layer.
## Project Architecture
* config folder for app config like http service url , websocket url.
* data folder for webservice response data and models , network class to send requests.
* ui folder contains app shared components & app screens organized by feature.
* util folder contains util classes like url query parser , etc...

### Design Patterns used in app.
* Singleton design pattern.
* Independent of framework concept.
* Single responsibility.


### Improvements
- Change QueryParamUtil class to custom hook function.
- move formik validation in new package inside each screen to hide complexity .